import React, { Component } from 'react'
import { REGISTRATION_FORM } from '../util/constants';
import { register } from '../util/services';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FontIcon from 'material-ui/FontIcon';
import { blue500, red500, greenA200 } from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import Preview from './uploadfile'
import Dropzone from 'react-dropzone';
import { BASE_URL, FormType } from '../util/constants';
import {
    Form,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    Col,
    Row,
    FormGroup,
    FormText,
    Label,
    Input,
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane,
} from 'reactstrap';
import StepForm from './StepForm.js';
import axios from 'axios';
const baseStyle = {
    textAlign: "-webkit-center",
    height: "60px",
    width: "60px",
    borderWidth: 1,
    borderColor: '#ced4da',
    borderStyle: 'double',
    borderRadius: 5,
};

const activeStyle = {
    borderStyle: 'solid',
    borderColor: '#6c6',
    backgroundColor: '#eee'
};

const rejectStyle = {
    borderStyle: 'solid',
    borderColor: '#c66',
    backgroundColor: '#eee'
}

class AddProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filesPreview: [],
            filesToBeSent: [],
            printcount: 10,
            productCode: "",
            // size: false
        }
    }

    handleChange = (e, elem) => {
        let _this = this;
        console.log("this state before change:", e.target.value, elem);
        this.setState({ [elem.name]: e.target.value }, function () {
            console.log("this state after change:", this.state)
            _this.validateField(elem);
        });
        console.log("this state after change:", this.state)
    }

    handlenumberChange = (e, elem) => {
        let _this = this;
        console.log("this state before change:", e.target.value, elem);
        this.setState({ [elem.name2]: e.target.value }, function () {
            console.log("this state after change:", this.state)
            _this.validateField(elem);
        });
        console.log("this state after change:", this.state)
    }

    componentWillMount = () => {
        let elements = {};
        let fields = {};
        ((REGISTRATION_FORM && REGISTRATION_FORM.elements) || []).forEach(element => {
            if (element.type == "colorUpload") {
                console.log("color array:", elements, element, elements[element], elements[element.name])
                elements[element.name] = [{ file: null, color: "" }];
            }
            else if (element.type == "sizeratio") {
                console.log("size array:", elements, element, elements[element], elements[element.name])
                elements[element.name] = [{ size: "", quantity: "" }];
            } else {
                elements[element.name] = "";
            }
            fields[element.name] = {
                hasError: "",
                message: ""
            }
        });
        this.setState({ ...elements, fields });
    }

    validateField = (element = {}) => {
        let props = element.props || {};
        let fields = { ...this.state.fields };
        let hasError = false;
        if (props.required) {
            if (!this.state[element.name]) {
                fields[element.name] = {
                    hasError: true,
                    message: "This field is mandatory"
                }
                hasError = true;
            }
        }
        if (hasError) {
            this.setState({ fields });
            return fields[element.name];
        } else {
            fields[element.name] = {
                hasError: false,
                message: ""
            }
            this.setState({ fields });
            return;
        }
    }

    validateForm = (form) => {
        console.log("validate form:", form)
        let _this = this;
        let hasError = false;
        let fields = { ...this.state.fields };
        (form.elements || []).forEach((elem, index) => {
            let valid = _this.validateField(elem);
            if (valid) {
                hasError = true;
                fields[elem.name] = valid;
            } else {
                fields[elem.name] = {
                    hasError: false,
                    message: ""
                };
            }
        });
        this.setState({ fields });
        return !hasError;
    }

    onSubmit = (e) => {
        e.preventDefault();
        let _this = this;
        console.log("this is the state when submited:", this.state)
        var valid = this.validateForm(REGISTRATION_FORM);
        console.log("valid....:", valid)
        if (this.validateForm(REGISTRATION_FORM)) {
            let data = { ...this.state };
            console.log("login data:", data);
            delete data.fields;
        }
        if (valid) {
            let imageUpload = []
            let n = this.state.LOI_FORM.length
            console.log("n in imageUpload:", n)
            for (var i = 0; i < n; i++) {
                if (this.state.LOI_FORM[i].file != null) {
                    imageUpload.push(this.state.LOI_FORM[i])
                }
            }
            console.log("imageupload:", imageUpload)
            let sizeUpload = []
            let s = this.state.SIZE_FORM.length
            console.log("n in imageUpload:", s)
            for (var i = 0; i < s; i++) {
                if (this.state.SIZE_FORM[i].size != "") {
                    sizeUpload.push(this.state.SIZE_FORM[i])
                }
            }
            console.log("imageupload:", sizeUpload)
            let reqobj = {
                "category": this.state.selectCategory,
                "subcategory": this.state.selectSubCategory,
                "type": this.state.selectType,
                "productCode": this.state.productCode,
                "productName": this.state.productName,
                "brandName": this.state.selectBrand,
                "retailPrice": this.state.retailPrice,
                "wholeSalePrice": this.state.wholesalePrice,
                "retailMOQ": this.state.retailMoq,
                "wholeSaleMOQ": this.state.wholesaleMoq,
                "quality": this.state.quailty,
                "stocks": this.state.stocks,
                "description": this.state.description,
                "productType": this.state.productType,
                "gstTax": this.state.gst,
                "dimensions": sizeUpload,
                "fileDescriptions": imageUpload
            }
            console.log("this is the reqobj when submited:", reqobj)
            axios.post(`http://localhost:3900/api/products/create`, reqobj, {
                headers: {
                    "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjYTBiNmMwOTEwMDcyNTg5NTNmYjk3NCIsImlhdCI6MTU1NDU2MjU1OCwiZXhwIjoxNTU0NjQ4OTU4fQ.4YR3t8tkDspKYlnrjkULTVUw2WIzub5yAYVT4HVM9vs"
                }
            }).then((response) => {
                console.log("respose:", response)
            }).catch((err) => {
                console.log(err);
            });
        }
    }


    onDrop = (acceptedFiles, rejectedFiles) => {
        console.log(acceptedFiles);
        console.log('Accepted files: ', acceptedFiles, acceptedFiles[0].name);
        // console.log("file to upload:",this.state.filesToBeSent)
        var filesToBeSent = acceptedFiles;
        console.log("files to be sent:", filesToBeSent)
        console.log("state:", this.state)
        if (filesToBeSent.length < this.state.printcount) {
            filesToBeSent.push(acceptedFiles);
            var filesPreview = [];
            for (var i = 0; i < filesToBeSent.length - 1; i++) {
                console.log("files preview for loop:", filesToBeSent.length, filesToBeSent[i].name)
                filesPreview.push(<div>
                    {filesToBeSent[i].name}
                    <MuiThemeProvider>
                        <a href="#"><FontIcon
                            className="material-icons customstyle"
                            color={blue500}
                            styles={{ top: 10, }}
                        >clear</FontIcon></a>
                    </MuiThemeProvider>
                </div>
                )
            }
            this.setState({ filesToBeSent, filesPreview });
        }
        else {
            alert("You have reached the limit of printing files at a time")
        }
    }

    handleClick(event) {
        console.log("handleClick", event);
        var self = this;
        if (this.state.filesToBeSent.length > 0) {
            var filesArray = this.state.filesToBeSent;
            console.log("file array:", filesArray)
            //   var req = request.post(apiBaseUrl+'fileupload');
            //   for(var i in filesArray){
            //       // console.log("files",filesArray[i][0]);
            //       req.attach(filesArray[i][0].name,filesArray[i][0])
            //   }
            //   req.end(function(err,res){
            //     if(err){
            //       console.log("error ocurred");
            //     }
            //     console.log("res",res);npm install react react-dom
            //     alert("File printing completed")
            //   });
        }
        else {
            alert("Please upload some files first");
        }
    }

    handleColorFilesDrop = (element, acceptedFiles, rejectedFiles, index) => {
        console.log("handleFiles = ", acceptedFiles, rejectedFiles);
        if (acceptedFiles && acceptedFiles.length) {
            let file = acceptedFiles[0];
            file = Object.assign(file, { preview: URL.createObjectURL(file) });
            console.log("File:", file);
            let colorsArray = this.state[element.name];
            colorsArray[index].file = file;
            this.setState({ [element.name]: colorsArray });
        }
    }

    handleColorNameChange = (e, element, index) => {
        let _this = this;
        let colorsArray = this.state[element.name];
        colorsArray[index].color = e.target.value;
        this.setState({ [element.name]: colorsArray }, function () {
            //_this.validateField(element);
        });
    }

    handleQuantityNameChange = (e, element, index) => {
        let _this = this;
        let sizeArray = this.state[element.name];
        sizeArray[index].quantity = e.target.value;
        this.setState({ [element.name]: sizeArray }, function () {
            //_this.validateField(element);
        });
    }

    handleSizeChange = (e, element, index) => {
        console.log("handleSizeChange", e.target.value, element, index)
        let _this = this;
        let sizeArray = this.state[element.name];
        console.log("handleSizeChange2", this.state);
        sizeArray[index].size = e.target.value;
        this.setState({ [element.name]: sizeArray }, function () {
            //_this.validateField(element);
        });
    }

    addColorDropRow = (element) => {
        let colorsArray = this.state[element.name];
        console.log("add color", colorsArray)
        colorsArray.push({
            file: null,
            color: ""
        })
        this.setState({ [element.name]: colorsArray }, function () {
            //_this.validateField(element);
        });
        console.log("this.state:", this.state)
    }

    addNumberDropRow = (element) => {
        console.log("addNumberDropRow:", this.state[element.name])
        let sizeArray = this.state[element.name];
        console.log("add color", sizeArray)
        // this.setState({ size: true })
        sizeArray.push({
            size: "",
            quantity: ""
        })
        this.setState({ [element.name]: sizeArray }, function () {
            //_this.validateField(element);
        });
        console.log("this.state:", this.state)
    }


    minusColorDropRow = (element) => {
        console.log("element:", element)
        let colorsArray = this.state[element.name];
        console.log("add color", colorsArray)
    }


    render() {
        let _this = this;
        return (
            <div style={{ margin: "20px" }}><span style={{ fontSize: "28px", color: "#67a6dc" }}>Add Product</span>
                <form className="user" noValidate onSubmit={this.onSubmit.bind(this)}>
                    <div className="form-row">
                        {
                            ((REGISTRATION_FORM && REGISTRATION_FORM.elements) || []).map((elem, index) => {
                                let custClass = `form-control form-control-user ${_this.state.fields[elem.name].hasError ? "is-invalid" : ""}`
                                console.log("element:", elem)
                                if (elem.type == "select") {
                                    return (
                                        <div className="col-md-6" key={`${elem.name}_${index}`}>
                                            <div className="form-group">
                                                <span>{elem.title}</span>
                                                <select className={custClass}
                                                    id={elem.name}
                                                    name={elem.name}
                                                    value={this.state[elem.name] || ""}
                                                    onChange={(e) => _this.handleChange(e, elem)}
                                                    {...elem.props}>
                                                    {
                                                        elem.options.map((ele, i) => {
                                                            return (<option key={`${ele.label}_${i}`}>{ele.value}</option>)
                                                        })
                                                    }
                                                </select>
                                                {
                                                    (_this.state.fields[elem.name].hasError) ?
                                                        (
                                                            <small className="form-text text-danger">{_this.state.fields[elem.name].message}</small>
                                                        ) : ""
                                                }
                                            </div>
                                        </div>
                                    )
                                }
                                else if (elem.type == "sizeratio" || (this.state.size == true)) {
                                    console.log("entering number")
                                    return (
                                        <div className="col-md-12">
                                            <div className="col-md-3">{elem.title}</div>
                                            <div className="col-md-12 row" style={{ display: "flex", marginTop: "10px" }}>
                                                {(this.state[elem.name] || [{ size: "", quantity: "" }]).map((sizes, index) => {
                                                    console.log('return size chooser', sizes)
                                                    return (
                                                        <div className="col-md-3" style={{ display: "flex", marginTop: "10px" }}>
                                                            <input
                                                                type="text"
                                                                className={`${custClass} col-md-4`}
                                                                id={elem.name}
                                                                name={elem.name}
                                                                value={sizes.size || ""}
                                                                placeholder={elem.placeholder}
                                                                autoComplete="new-password"
                                                                onChange={(e) => _this.handleSizeChange(e, elem, index)}
                                                                {...elem.props} />
                                                            <input
                                                                type={elem.type2}
                                                                className={`${custClass} col-md-5`}
                                                                style={{ marginLeft: "5px" }}
                                                                id={elem.name2}
                                                                placeholder={elem.placeholder2}
                                                                name={elem.name2}
                                                                value={sizes.quantity || ""}
                                                                autoComplete="new-password"
                                                                onChange={(e) => _this.handleQuantityNameChange(e, elem, index)}
                                                                {...elem.props} />
                                                            <div className="col-2 text-center">
                                                                {
                                                                    (index == ((this.state[elem.name] || [{ size: null, quantity: "" }]).length - 1)) ? (
                                                                        <i className="fa fa-plus" onClick={() => _this.addNumberDropRow(elem)}></i>
                                                                    ) : (
                                                                            <i className="fa fa-minus" onClick={() => _this.minusColorDropRow(elem)}></i>
                                                                        )
                                                                }
                                                            </div>
                                                        </div>
                                                        // {
                                                        //     (_this.state.fields[elem.name].hasError) ?
                                                        //         (
                                                        //             <small className="form-text text-danger">{_this.state.fields[elem.name].message}</small>
                                                        //         ) : ""
                                                        // }
                                                    )
                                                })
                                                }
                                            </div>

                                        </div>
                                    )
                                }

                                else if (elem.type == "colorUpload") {
                                    console.log("color chooser:", elem.type)
                                    var type = elem.type;
                                    // return(
                                    //     <div>
                                    //         <span>A</span>
                                    //     </div>
                                    // )
                                    return (
                                        <div className="col-md-12" style={{ padding: "0px", marginTop: "20px", marginBottom: "20px" }}  >
                                            <div style={{ paddingLeft: "10px" }}>Image List</div>
                                            <div className="row" style={{display:"flex"}}>
                                            {(this.state[elem.name] || [{ file: null, color: "" }]).map((item, index) => {
                                                console.log('return color chooser')
                                                return (

                                                    // <React.Fragment>
                                                    //     <StepForm
                                                    //         form={elem.LOI_FORM}
                                                    //         type={elem.LOI_FORM.steps[0].elements[0].type}
                                                    //         key={this.state.timeStamp} />
                                                    // </React.Fragment>
                                                    <div className="col-md-6 row" style={{ marginTop: "10px" }}>

                                                        <div className="col-md-3">
                                                            <Dropzone
                                                                onDrop={(accept, reject) => this.handleColorFilesDrop(elem, accept, reject, index)}>
                                                                {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => {
                                                                    let styles = { ...baseStyle }
                                                                    styles = isDragActive ? { ...styles, ...activeStyle } : styles
                                                                    styles = isDragReject ? { ...styles, ...rejectStyle } : styles
                                                                    // styles = props.required ? { ...styles, ...requiredStyle } : styles

                                                                    return (
                                                                        <div
                                                                            {...getRootProps()}
                                                                            style={styles}
                                                                        >
                                                                            <input {...getInputProps()} />
                                                                            <div style={
                                                                                {
                                                                                    padding: "0px 15px",
                                                                                    lineHeight: "38px",
                                                                                    color: "#495057",
                                                                                    fontSize: "1rem"
                                                                                }
                                                                            }>
                                                                                {
                                                                                    item ? (
                                                                                        (item.file instanceof File) ? (
                                                                                            <p>
                                                                                                <img
                                                                                                    src={item.file.preview}
                                                                                                    placeholder="click here"
                                                                                                    style={
                                                                                                        {
                                                                                                            marginTop: "7px",
                                                                                                            marginLeft: "-5px",
                                                                                                            height: "40px", borderRadius: "50%",
                                                                                                            width: "40px"
                                                                                                        }
                                                                                                    }
                                                                                                />
                                                                                            </p>
                                                                                        ) : (item.file && typeof item.file == "object") ? (
                                                                                            <p><img src={`${BASE_URL}/getDocument?path=${item.file.path}`} style={
                                                                                                {
                                                                                                    width: "60px",
                                                                                                    height: "60px",
                                                                                                    borderRadius: "50%"
                                                                                                }
                                                                                            } /> {item.file.originalname}</p>
                                                                                        ) : ""
                                                                                    ) : (isDragAccept ? 'Drop files here...' : 'Drag files here...')
                                                                                }
                                                                            </div>
                                                                            {isDragReject && <div>Unsupported file type...</div>}
                                                                        </div>
                                                                    )
                                                                }}
                                                            </Dropzone>
                                                        </div>
                                                        <div className="col-6">
                                                            <Input
                                                                type={type}
                                                                value={item.color || ""}
                                                                placeholder={item.placeholder}
                                                                // className={classname}
                                                                onChange={(e) => _this.handleColorNameChange(e, elem, index)} />
                                                        </div>
                                                        <div className="col-2 text-center">
                                                            {
                                                                (index == ((this.state[elem.name] || [{ file: null, color: "" }]).length - 1)) ? (
                                                                    <i className="fa fa-plus" onClick={() => _this.addColorDropRow(elem)}></i>
                                                                ) : (
                                                                        <i className="fa fa-minus" onClick={() => _this.minusColorDropRow(elem)}></i>
                                                                    )
                                                            }
                                                        </div>
                                                    </div>

                                                )
                                            })
                                            }
                                        </div>
                                        </div>
                                    )
                                }
                                else if (elem.type == "textarea") {
                                    return (
                                        <div className="col-md-6" key={`${elem.name}_${index}`}>
                                            <div className="form-group">
                                                <span>{elem.title}</span>
                                                <textarea
                                                    className={`${custClass}`}
                                                    id={elem.name}
                                                    name={elem.name}
                                                    value={_this.state[elem.name] || ""}
                                                    autoComplete="new-password"
                                                    onChange={(e) => _this.handleChange(e, elem)}
                                                    {...elem.props}>
                                                </textarea>
                                            </div>
                                        </div>
                                    )
                                }
                                else if (elem.type == "text") {
                                    return (
                                        <div className="col-md-6">
                                            <div
                                                className="form-group"
                                                key={`${elem.name}_${index}`}>
                                                <span>{elem.title}</span>
                                                <input
                                                    type={elem.type}
                                                    className={custClass}
                                                    id={elem.name}
                                                    name={elem.name}
                                                    value={this.state[elem.name] || ""}
                                                    autoComplete="new-password"
                                                    onChange={(e) => _this.handleChange(e, elem)}
                                                    {...elem.props} />
                                                {
                                                    (_this.state.fields[elem.name].hasError) ?
                                                        (
                                                            <small className="form-text text-danger">{_this.state.fields[elem.name].message}</small>
                                                        ) : ""
                                                }
                                            </div>
                                        </div>
                                    )
                                }

                            })
                        }
                    </div>
                    {
                        this.state.errMessage ? (
                            <p className="text-center text-danger"><strong>{this.state.errMessage}</strong></p>
                        ) : ""
                    }
                    <button
                        type="submit"
                        className="btn btn-warning btn-user btn-block"
                        onClick={() => _this.onSubmit.bind(this)}
                        style={{ color: "#fff", background: "#9C3", border: "none" }}
                    >Add Product</button>
                </form>
            </div>
        )
    }
}

export default AddProduct