import React from 'react';

import { 
    Form, 
    Card, 
    CardHeader, 
    CardBody,
    CardFooter, 
    Button,
    Col, 
    Row,
    FormGroup,
    FormText,
    Label,
    Input,
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane, 
} from 'reactstrap';
import Select from 'react-select';
import classnames from 'classnames';
import Dropzone from 'react-dropzone';
import { BASE_URL, FormType } from '../util/constants';
import '../styles/form.scss';

const baseStyle = {
    width: "100%",
    height: "38px",
    borderWidth: 1,
    borderColor: '#ced4da',
    borderStyle: 'double',
    borderRadius: 5,
};

const activeStyle = {
    borderStyle: 'solid',
    borderColor: '#6c6',
    backgroundColor: '#eee'
};

const rejectStyle = {
    borderStyle: 'solid',
    borderColor: '#c66',
    backgroundColor: '#eee'
}

const requiredStyle = {
    borderLeft: "solid 3px red"
}

class StepForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            "activeTab": 1,
            "tenureOfAggrement": "15 Years (3 + 3 + 3 + 3 + 3)",
            "fields": {}
        };
    }

    componentWillMount = () => {
        let _this = this;
        let fields = {};
        let actualFields = {};
        if (this.props.isEdit) {
            console.log("EditDetails: ", this.props.editDetail);
        }
        if (this.props.type == FormType.STEP) {
            ((this.props.form && this.props.form.steps) || []).forEach((step, stepIndex) => {
                let fdta = _this.getFormFields(step.elements || []);
                fields = {...fdta.fields};
                actualFields = {...actualFields, ...fdta.actualFields}
            });
        } else {
            let fdta = _this.getFormFields((this.props.form && this.props.form.elements) || []);
            fields = {...fdta.fields};
            actualFields = {...fdta.actualFields}
        }
        console.log("ActialFields = ", {...actualFields, fields});
        this.setState({...actualFields, fields}, function() {
            if(_this.props.isEdit && _this.props.editDetail) {
                window.scrollTo(0, 0);
            }
        });
    }

    getFormFields = (elements = []) => {
        let _this = this;
        let fields = {};
        let actualFields = {};
        (elements || []).forEach((element, elementIndex) => {
            fields[element.name] = {
                hasError: false,
                message: ""
            };
            if(element.type == "color-chooser"){
                actualFields[element.name] = [{file: null, color: ""}];
            }
            if (_this.props.isEdit && _this.props.editDetail) {
                if (element.type == "select") {
                    actualFields[element.name] = {
                        value: _this.props.editDetail[element.name],
                        label: _this.props.editDetail[element.name]
                    };
                } else if(element.type == "color-chooser"){
                    actualFields[element.name] = [{file: null, color: ""}];
                } else {
                    actualFields[element.name] = _this.props.editDetail[element.name];
                }
            }
        });
        console.log("ActualFiels: ", actualFields);
        return {
            fields,
            actualFields
        }
    }

    handleSubmit = () => {
        let flag = false;
        if (this.props.type === FormType.STEP) {
            flag = this.validateTab(((this.props.form && this.props.form.steps) || []).length)
        } else {
            flag = this.validateDefault((this.props.form && this.props.form.elements) || []);
        }
        console.log("FLAG = ", flag);
        if (flag) {
            if (this.props.onSubmit) {
                let data = {...this.state};
                delete data.activeTab;
                delete data.fields;
                for(let key in data) {
                    if (data[key] instanceof File) {
                        data[key] = data[key];
                    } else if (typeof data[key] == "object") {
                        data[key] = data[key].value || "";
                    }
                }
                console.log("Data to submit = ", data);
                this.props.onSubmit(data);
            } else {
                console.warn("OnSubmit not provided");
            }
        } 
        /*
        if (this.validateTab(((FORM_LIST && FORM_LIST.steps) || []).length)) {
            if (this.props.onSubmit) {
                let data = {...this.state};
                delete data.activeTab;
                delete data.fields;
                for(let key in data) {
                    if (data[key] instanceof File) {
                        data[key] = data[key];
                    } else if (typeof data[key] == "object") {
                        data[key] = data[key].value || "";
                    }
                }
                this.props.onSubmit(data);
            } else {
                console.warn("OnSubmit not provided");
            }
        }
        */
    }

    handleChange = (e, element) => {
        let _this = this;
        this.setState({[element.name]: e.target.value}, function() {
            _this.validateField(element);
        });
    }

    handleSelectChange = (element, value) => {
        let _this = this;
        this.setState({[element.name]: value}, function() {
            _this.validateField(element);
        });
    }

    handleFileDrop = (element, acceptedFiles, rejectedFiles) => {
        //let _this = this;
        console.log("handleFiles = ", acceptedFiles, rejectedFiles);
        if (acceptedFiles && acceptedFiles.length) {
            let file = acceptedFiles[0];
            file = Object.assign(file, {preview: URL.createObjectURL(file)});
            console.log("File:", file);
            this.setState({[element.name]: file}, function() {
               // _this.validateField(element);
            });
        }
    }

    handleColorFilesDrop = (element, acceptedFiles, rejectedFiles, index) => {
        console.log("handleFiles = ", acceptedFiles, rejectedFiles);
        if (acceptedFiles && acceptedFiles.length) {
            let file = acceptedFiles[0];
            file = Object.assign(file, {preview: URL.createObjectURL(file)});
            console.log("File:", file);
            let colorsArray = this.state[element.name];
            colorsArray[index].file = file;
            this.setState({[element.name]: colorsArray});
        }
    }

    handleColorNameChange = (e, element, index) => {
        let _this = this;
        let colorsArray = this.state[element.name];
        colorsArray[index].color = e.target.value;
        this.setState({[element.name]: colorsArray}, function() {
            //_this.validateField(element);
        });
    }

    addColorDropRow = (element) => {
        let colorsArray = this.state[element.name];
        colorsArray.push({
            file: null,
            color: ""
        })
        this.setState({[element.name]: colorsArray}, function() {
            //_this.validateField(element);
        });
    }

    validateField = (element) => {
        let fields = {...this.state.fields};
        let props = element.props || {};
        let field = {
            hasError: false,
            message: ""
        };
        let hasError = false;
        if (props.required) {
            if (!this.state[element.name]) {
                hasError = true;
                field = {
                    hasError: true,
                    message: "This field is required."
                }
            }
        }
        console.log("%s : ", element.name, this.state[element.name], !this.state[element.name], field);
        fields[element.name] = field;
        this.setState({fields});
        if (hasError) {
            return field;
        } else {
            return false
        }
    }

    validateTab = (tabId) => {
        let _this = this;
        let list = (this.props.form && this.props.form.steps) || [];
        let hasError = false;
        let fields = {...this.state.fields};
        if (tabId <= list.length) {
            list[tabId - 1].elements.forEach(element => {
                if (element.type !== "break") {
                    let validate = _this.validateField(element);
                    if (validate) {
                        hasError = true;
                        fields[element.name] = validate;
                    } else {
                        fields[element.name] = {
                            hasError: false,
                            message: ""
                        }
                    }
                }
            });
        }
        this.setState({fields});
        return !hasError;
    }

    validateDefault = (elements = []) => {
        let _this = this;
        let hasError = false;
        let fields = {...this.state.fields};
        elements.forEach(element => {
            if (element.type !== "break") {
                let validate = _this.validateField(element);
                if (validate) {
                    hasError = true;
                    fields[element.name] = validate;
                } else {
                    fields[element.name] = {
                        hasError: false,
                        message: ""
                    }
                }
            }
        });
        this.setState({fields});
        return !hasError;
    }

    validateFrom = () => {

    }

    renderForm = (list = []) => {
        let _this = this;
        return (
            <Row form>
                {list.map((element, index) => {
                    return _this.renderField(element)
                })}
            </Row>
        )
    }

    renderField = (element = {}) => {
        let _this = this;
        let type = element.type;
        let props = element.props || {};
        let input = "";
        let classname = `${props.required ? 'field-required' : ''}`;

        switch(type) {
            case "color-chooser": {
            console.log("entering color-chooser")
                input = (
                    <FormGroup key={`form-element-${element.name}`}>
                        <Label for={element.name}>{element.label}</Label>
                        <div className="dropdown-divider"></div>
                        {
                            (this.state[element.name] || [{file: null, color: ""}]).map((item, index) => {
                                return (
                                    <div className="row">
                                        <div className="col-4">
                                            <Dropzone 
                                                onDrop={(accept, reject) => this.handleColorFilesDrop(element, accept, reject, index)}>
                                            {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => {
                                                let styles = {...baseStyle}
                                                styles = isDragActive ? {...styles, ...activeStyle} : styles
                                                styles = isDragReject ? {...styles, ...rejectStyle} : styles
                                                styles = props.required ? {...styles, ...requiredStyle} : styles

                                                return (
                                                    <div
                                                        {...getRootProps()}
                                                        style={styles}
                                                    >
                                                        <input {...getInputProps()} />
                                                        <div style={
                                                            {
                                                                padding: "0px 15px",
                                                                lineHeight: "38px",
                                                                color: "#495057",
                                                                fontSize: "1rem"
                                                            }
                                                        }>
                                                        {
                                                            item ? (
                                                                (item.file instanceof File) ? (
                                                                    <p>BASE_URL
                                                                        <img 
                                                                            src={item.file.preview}
                                                                            style={
                                                                                {
                                                                                    height: "40px", borderRadius: "50%",
                                                                                    width: "40px"
                                                                                }
                                                                            }
                                                                            />
                                                                    </p>
                                                                ) : (item.file && typeof item.file == "object") ? (
                                                                    <p><img src={`${BASE_URL}/getDocument?path=${item.file.path}`} style={
                                                                        {
                                                                            width: "20px",
                                                                            height: "20px",
                                                                            borderRadius: "50%"
                                                                        }
                                                                    }/> {item.file.originalname}</p>
                                                                ) : ""
                                                            ) : (isDragAccept ? 'Drop files here...' : 'Drag files here...')
                                                        }
                                                        </div>
                                                        {isDragReject && <div>Unsupported file type...</div>}
                                                    </div>
                                                )
                                            }}
                                        </Dropzone>
                                        </div>
                                        <div className="col-6">
                                        <Input 
                                            type={type} 
                                            value={item.color || ""}
                                            className={classname}
                                            onChange={(e) => _this.handleColorNameChange(e, element, index)}/>
                                        </div>
                                        <div className="col-2 text-center">
                                            {
                                               (index == ((this.state[element.name] || [{file: null, color: ""}]).length - 1)) ? (
                                                <i className="fas fa-fw fa-plus" onClick={() => _this.addColorDropRow(element)}></i>
                                               ) : (
                                                <i className="fas fa-fw fa-minus"></i>
                                               )
                                            }
                                        </div>
                                    </div>
                                )
                            })
                            
                        }
                    </FormGroup>
                )
                break;
            }
            case "select": {
                input = (
                    <FormGroup key={`form-element-${element.name}`}>
                        <Label for={element.name}>{element.label}</Label>
                        <Select 
                            name={element.name}
                            isClearable={true} 
                            isSearchable={true} 
                            onChange={(value) => this.handleSelectChange(element, value)}
                            options={element.options}
                            className={classname}
                            classNamePrefix="customised-select"
                            value={this.state[element.name] || ""}
                            styles={{menu: (provided, state) => {
                                return {...provided, zIndex: 999}
                            }}}
                            {...element.props}
                        >
                        </Select>
                        {
                            (this.state.fields[element.name] && 
                                this.state.fields[element.name].hasError) ? 
                            <FormText color="danger">{this.state.fields[element.name].message}</FormText> : ""
                        }
                    </FormGroup>
                )
                break;
            }
            case "break": {
                input = (
                    <React.Fragment key={`form-element-${element.title}`}>
                        <p><strong>{element.title}</strong></p>
                        <hr />
                    </React.Fragment>

                )
                break;
            }
            case "file": {
                input = (
                    <FormGroup key={`form-element-${element.name}`}>
                        <Label for={element.name}>{element.label}</Label>
                        <Dropzone onDrop={(accept, reject) => this.handleFileDrop(element, accept, reject)}>
                            {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => {
                                let styles = {...baseStyle}
                                styles = isDragActive ? {...styles, ...activeStyle} : styles
                                styles = isDragReject ? {...styles, ...rejectStyle} : styles
                                styles = props.required ? {...styles, ...requiredStyle} : styles

                                return (
                                    <div
                                        {...getRootProps()}
                                        style={styles}
                                    >
                                        <input {...getInputProps()} />
                                        <div style={
                                            {
                                                padding: "0px 15px",
                                                lineHeight: "38px",
                                                color: "#495057",
                                                fontSize: "1rem"
                                            }
                                        }>
                                        {
                                            this.state[element.name] ? (
                                                (this.state[element.name] instanceof File) ? (
                                                    <p>{this.state[element.name].name}</p>
                                                ) : (typeof this.state[element.name] == "object") ? (
                                                    <p><img src={`${BASE_URL}/getDocument?path=${this.state[element.name].path}`} style={
                                                        {
                                                            width: "20px",
                                                            height: "20px",
                                                            borderRadius: "50%"
                                                        }
                                                    }/> {this.state[element.name].originalname}</p>
                                                ) : ""
                                            ) : (isDragAccept ? 'Drop files here...' : 'Drag files here...')
                                        }
                                        </div>
                                        {isDragReject && <div>Unsupported file type...</div>}
                                    </div>
                                )
                            }}
                        </Dropzone>
                    </FormGroup>
                )
                break;
            }
            default: {
                input = (
                    <FormGroup key={`form-element-${element.name}`}>
                        <Label for={element.name}>{element.label}</Label>
                        <Input 
                            type={type} 
                            name={element.name} 
                            id={element.name}
                            {...element.props} 
                            value={this.state[element.name] || ""}
                            className={classname}
                            onChange={(e) => this.handleChange(e, element)}/>
                        {
                            (this.state.fields[element.name] && 
                                this.state.fields[element.name].hasError) ? 
                                <FormText color="danger">{this.state.fields[element.name].message}</FormText> : ""
                        }
                    </FormGroup>
                )
            }
        }

        let columProps = {
            xs: 12,
            sm: 12,
            md: 6,
            lg: 6,
            xl: 6
        }
        if (element.width) {
            columProps = {...columProps, ...element.width};
        }
        if (type === "break") {
            columProps = {
                xs: 12,
                sm: 12,
                md: 12,
                lg: 12,
                xl: 12
            }
            return (
                <Col {...columProps} key={`form-column-${element.title}`}>
                {input}
                </Col>
            )
        } else {
            return (
                <Col {...columProps} key={`form-column-${element.name}`}>
                    {input}
                </Col>
            )
        }
    }

    toggleTab = (tab) => {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
    }

    validateTabAndMove = (tabId) => {
        if (this.validateTab(tabId)) {
            this.toggleTab(tabId + 1);
            window.scrollTo(0, 0);
        }
    }

    renderNAV = (formSteps = []) => {
        let _this = this;
        console.log("formsteps:",formSteps)
        return (
            <Nav pills>
                {
                    formSteps.map((step, stepIndex) => {
                        if (_this.props.isEdit) {
                            return (
                                <NavItem key={`step_nav_${stepIndex}`}>
                                    <NavLink
                                    className={classnames({ active: this.state.activeTab === (stepIndex + 1) })}>
                                    {step.title}
                                    </NavLink>
                                </NavItem>
                            )
                        } else {
                            if (!step.onEdit) {
                                return (
                                    <NavItem key={`step_nav_${stepIndex}`}>
                                        <NavLink
                                        className={classnames({ active: this.state.activeTab === (stepIndex + 1) })}>
                                        {step.title}
                                        </NavLink>
                                    </NavItem>
                                )
                            } else {
                                return ""
                            }
                        }
                    })
                }
            </Nav>
        )
    }

    renderNavButtons = (formSteps = []) => {
        let _this = this;
        let buttonAligns = {
            xs: 6,
            sm: 6,
            md: 3,
            lg: 3,
            xl: 3
        }
        if (formSteps.length === 1) {
            return (
                <React.Fragment>
                    <Col {...buttonAligns}>
                        <Button 
                            block={true} 
                            outline={true} 
                            type="button"
                            onClick={() => this.handleSubmit()}>Submit</Button>
                    </Col>
                </React.Fragment>
            )
        }
        if (_this.state.activeTab === 1) {
            return (
                <React.Fragment>
                    <Col {...buttonAligns}>
                        <Button 
                            block={true} 
                            outline={true} 
                            type="button" 
                            onClick={()=>this.validateTabAndMove(_this.state.activeTab)}>Next</Button>
                    </Col>
                </React.Fragment>
            )
        } else if (_this.state.activeTab === formSteps.length) {
            return (
                <React.Fragment>
                    <Col {...buttonAligns}>
                        <Button 
                            block={true} 
                            outline={true} 
                            type="button" 
                            onClick={()=>this.toggleTab(_this.state.activeTab - 1)}>Previous</Button>
                    </Col>
                    <Col {...buttonAligns}>
                        <Button 
                            block={true} 
                            outline={true} 
                            type="button"
                            onClick={() => this.handleSubmit()}>Submit</Button>
                    </Col>
                </React.Fragment>
            )
        } else {
            return (
                <React.Fragment>
                    <Col {...buttonAligns}>
                        <Button 
                            block={true} 
                            outline={true} 
                            type="button" 
                            onClick={()=>this.toggleTab(_this.state.activeTab - 1)}>Previous</Button>
                    </Col>
                    <Col {...buttonAligns}>
                        <Button 
                            block={true} 
                            outline={true} 
                            type="button" 
                            onClick={()=>this.validateTabAndMove(_this.state.activeTab)}>Next</Button>
                    </Col>
                </React.Fragment>
            )
        }
    }

    renderLayout = () => {
        let _this = this;
        let layout = "";
        console.log("this.props.form:",this.props.form,this.props.type)
        switch(this.props.type) {
            case FormType.STEP: {
                layout = (
                    <React.Fragment key="step_form">
                        {
                            this.renderNAV((this.props.form && this.props.form.steps) || [])
                        }
                        <TabContent activeTab={this.state.activeTab}>
                            {
                                ((this.props.form && this.props.form.steps) || []).map((step, stepIndex) => {
                                    if (_this.props.isEdit) {
                                        return (
                                            <TabPane 
                                                tabId={stepIndex + 1} 
                                                key={`step_${stepIndex}`}>
                                            <br/>
                                                {_this.renderForm(step.elements)}
                                            </TabPane>
                                        )
                                    } else {
                                        if (!step.onEdit) {
                                            return (
                                                <TabPane 
                                                    tabId={stepIndex + 1} 
                                                    key={`step_${stepIndex}`}>
                                                <br/>
                                                    {_this.renderForm(step.elements)}
                                                </TabPane>
                                            )
                                        } else {
                                            return ""
                                        }
                                    }
                                })
                            }
                        </TabContent>
                    </React.Fragment>
                )
                break;
            }
            case FormType.SEARCH: {
                break;
            }
            default: {
                layout = (
                    <React.Fragment key="default_form">
                        {
                            this.renderForm((this.props.form && this.props.form.elements) || [])
                        }
                    </React.Fragment>
                )
                break;
            }
        }
        return layout;
    }

    renderFormButtons = () => {
        let _this = this;
        let layout = "";
        let buttonAligns = {
            xs: 6,
            sm: 6,
            md: 3,
            lg: 3,
            xl: 3
        }
        switch(this.props.type) {
            case FormType.STEP: {
                layout = _this.renderNavButtons((this.props.form && this.props.form.steps) || []);
                break;
            }
            case FormType.SEARCH: {
                layout = (
                    <React.Fragment>
                        <Col {...buttonAligns}>
                            <Button 
                                block={true} 
                                outline={true} 
                                type="button"
                                onClick={() => this.handleSubmit()}>Search</Button>
                        </Col>
                    </React.Fragment>
                )
                break;
            }
            default: {
                layout = (
                    <React.Fragment>
                        <Col {...buttonAligns}>
                            <Button 
                                block={true} 
                                outline={true} 
                                type="button"
                                onClick={() => this.handleSubmit()}>Submit</Button>
                        </Col>
                    </React.Fragment>
                )
                break;
            }
        }

        return layout;
    }

    render = () => {
        let _this = this;
        return (
            <React.Fragment>
                <div className="container">
                    <Form>
                        <Card>
                            <CardHeader className="text-center h5">{this.props.isEdit ? "Edit LOI" : "Letter of Intent"}</CardHeader>
                            <CardBody>
                                {
                                    this.renderLayout()
                                }
                            </CardBody>
                            <CardFooter className="text-muted">
                                <Row form className="justify-content-end">
                                    {this.renderFormButtons()}
                                </Row>
                            </CardFooter>
                        </Card>
                    </Form>
                </div>
            </React.Fragment>
        )
    }
}

export default StepForm;