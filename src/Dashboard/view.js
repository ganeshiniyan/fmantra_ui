import React, { Component } from 'react'
import {
    Button,
    Badge,
    ListGroup,
    ListGroupItem,
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import $ from 'jquery';
import Home from './Home';
import { Route, Redirect, BrowserRouter, Switch, Link } from 'react-router-dom'
import '../App.css';

const items = [
    {
        src: 'Images/blue.jpg',
        altText: 'Slide 1',
        caption: 'Slide 1'
    },
    {
        src: 'Images/blue.jpg',
        altText: 'Slide 2',
        caption: 'Slide 2'
    },
    {
        src: 'Images/blue.jpg',
        altText: 'Slide 3',
        caption: 'Slide 3'
    }
];

const src = 'https://images.unsplash.com/photo-1444065381814-865dc9da92c0'
// const src='Images/blue.jpg'

const price = [
    { mrp: "2000rs" },
    { price: "2000rs" },
    { save: "2000rs" }
]

class View extends Component {

    constructor(props) {
        super(props)
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
        this.menubar = this.menubar.bind(this);
        this.addkartroute = this.addkartroute.bind(this);
        this.state = {
            activeIndex: 0,
            backgroundImage: `url(${src})`,
            backgroundPosition: '0% 0%',
            isOpen: false,
            menuClose: false,
        }
    }

    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }

    handleMouseMove = e => {
        const { left, top, width, height } = e.target.getBoundingClientRect()
        const x = (e.pageX - left) / width * 100
        const y = (e.pageY - top) / height * 100
        this.setState({ backgroundPosition: `${x}% ${y}%` })
    }

    menubar(event) {
        console.log("clicking menu:", this.state.menuClose)
        // document.getElementsByClassName('menu-list')[0].style.visibility = 'hidden';
        if (this.state.menuClose === false) {
            $(".menu-list").hide();
            $('.main-char-div').removeClass("col-md-10")
            $('.main-char-div').addClass("col-md-12")
            this.setState({ menuClose: true });
        }
        else if (this.state.menuClose === true) {
            $(".menu-list").show();
            $('.main-char-div').removeClass("col-md-12")
            $('.main-char-div').addClass("col-md-10")
            this.setState({ menuClose: false })
        }
    }

    addkartroute(){
        console.log("redirecting")
        // return <Redirect to="/addtokart" />
        window.location.href=`http://localhost:3000/addtoKart`
    }

    render() {
        const { activeIndex } = this.state;

        const slides = items.map((item) => {
            return (
                <CarouselItem
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                >
                    <figure onMouseMove={this.handleMouseMove} style={this.state}>
                        <img src={src} alt={item.altText} style={{ width: "100%" }} />
                    </figure>

                    {/* <CarouselCaption captionText={item.caption} captionHeader={item.caption} /> */}
                </CarouselItem>
            );
        });
        return (
            <div>
                <div className="app-bar">
                    <div className="col-md-2 headers">
                        <img className="dashboardlogo" src="Images/dummy logo.svg" alt="dummy" />
                    </div>
                    <div className="col-md-10 headers">
                        <div className="app-bar-button">
                            <span>
                                <i class="fa fa-bars" onClick={this.menubar} aria-hidden="true"></i>
                                <label className="menu-title">Dashboard</label>
                            </span>
                            <div className="app-bar-icons">
                                <span className="notification">
                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                    <Badge color="danger">4</Badge>
                                </span>
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main-dashboard">
                    <div className="col-md-2 menu-list">
                        <ul>
                            <li className="nav-item active">
                                <Link to="/home" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>Home</span>
                                </Link>
                                <Link to="/view" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>View</span>
                                </Link>
                                <Link to="/addtokart" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>AddToKart</span>
                                </Link>
                            </li>
                        </ul>
                        {/* <ListGroup>
                            <ListGroupItem className="list-item" active tag="a" href="/dashboard" action>Home</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Dapibus ac facilisis in</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Morbi leo risus</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Porta ac consectetur ac</ListGroupItem>
                            <ListGroupItem className="list-item" disabled tag="a" href="/dashboard" action>Vestibulum at eros</ListGroupItem>
                        </ListGroup> */}

                        <BrowserRouter>
                            <Switch>
                                <Route exact path="/Home" component={Home} />
                                {/* <Route path='/' render={()=>
                          <Redirect to="/dashboard" />} /> */}
                            </Switch>
                        </BrowserRouter>
                    </div>
                    {/* <div className="col-md-10 main-char-div" style={{ display: "flex" }}> */}
                    {/* <div className="chart-div">
                            <div className="col-md-6 bar-chart">
                                <Barchart />
                            </div>
                            <div className="col-md-6 doughnut-chart">
                                <Doughnut />
                            </div>
                        </div>
                        <div className="chart-div area-chart">
                            <Areachart />
                        </div> */}

                    {/* </div> */}
                    <div className="col-md-10 main-char-div" style={{ padding: "0px" }}>
                        <div className="col-md-12 view" style={{ display: "flex", padding: "0px" }}>
                            <div className="col-md-7">
                                <Carousel
                                    activeIndex={activeIndex}
                                    next={this.next}
                                    interval={false}
                                    previous={this.previous}>
                                    <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                                    {slides}
                                    <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                                    <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
                                </Carousel>
                            </div>
                            <div className="col-md-5">
                                <span className="item-name" style={{ fontSize: "24px" }}>Yellow t-shirt</span>
                                <div>
                                    <a href="/home" style={{ fontFamily: "sans-serif" }}>Brand: JUMP USA</a>
                                    <div style={{ display: "flex" }}>
                                        <div>
                                            3.5
                                    </div>
                                        <div>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <div>
                                            (7)
                                    </div>
                                    </div>
                                </div>
                                <div>
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </div>
                                <div>
                                    <div>SELECT SIZE
                                </div>
                                    <div style={{ display: "flex" }}>
                                        <div className="Sizenumber">
                                            32
                                    </div>
                                        <div className="Sizenumber">
                                            34
                                    </div>
                                        <div className="Sizenumber">
                                            36
                                    </div>
                                        <div className="Sizenumber">
                                            40
                                    </div>
                                        <div className="Sizenumber">
                                            42
                                    </div>
                                    </div>
                                    <div>
                                        <div style={{ display: "flex" }}>
                                            <span style={{
                                                display: "block",
                                                width: "120px"
                                            }}>MRP :</span>
                                            <span>{price[0].mrp}</span>
                                        </div>
                                        <div style={{ display: "flex" }}>
                                            <span style={{
                                                display: "block",
                                                width: "120px"
                                            }}>Price :</span>
                                            <span>{price[1].price}</span>
                                        </div>
                                        <div style={{ display: "flex" }}>
                                            <span style={{
                                                display: "block",
                                                width: "120px"
                                            }}>You Save :</span>
                                            <span>{price[2].save}</span>
                                        </div>
                                    </div>
                                    <div>
                                        <Button color="primary" onClick={this.addkartroute} style={{ width: "100%" }}>ADD TO CART</Button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            // =================
            )
    }

}

export default View; 