import React, { Component } from 'react'
import CanvasJSReact from '../canvasjs-2.3.1/canvasjs.react';
import axios from 'axios';
// var CanvasJSReact = require('../canvasjs-2.3.1/canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Barchart extends Component {
    componentDidMount() {
        // document.getElementsByClassName("canvasjs-chart-credit").style.visibility = 'hidden';
        console.log("calling the axios")
        this.Bargraph((err, x) => {
            console.log("data in mount callback:", err, x)
            // this.setState({ chartdata: x });
            console.log("my state", this.state)
            this.setState({
                barchart: {
                    title: {
                        text: "Performance"
                    },
                    data: [
                        {
                            // Change type to "doughnut", "line", "splineArea", etc.
                            type: "column",
                            dataPoints: x
                        }
                    ]
                }
            })
        });

    }
    constructor(props) {
        super(props)
        this.Bargraph = this.Bargraph.bind(this);
        //this.convertdata=this.convertdata.bind(this);
        this.state = {
            barchart: {
                title: {
                    text: "Performance"
                },
                data: [
                    {
                        // Change type to "doughnut", "line", "splineArea", etc.
                        type: "column",
                        dataPoints: [
                            { label: "Nokia", y: 10 },
                            { label: "Motorola", y: 15 },
                            { label: "Samsung", y: 25 },
                            { label: "iphone", y: 30 },
                            { label: "Mi", y: 28 }
                        ]
                    }
                ]
            },
            chartdata: []
        }

    }
    Bargraph(chart) {
        axios.get('http://localhost:3001/barchart')
            .then((response) => {
                console.log("data:", response.data[0]);
                var chart_data = this.convertdata(response.data[0]);
                console.log("chart_data data:", chart_data)
                chart(null, chart_data);
            })
            .catch((error) => {
                console.log(error);
                chart(error);
            });
    }

    convertdata(data) {
        var datapoints = [];
        console.log("data in coverdata:", data)
        var label = data.label
        console.log("label1:", label)
        var labels
        var ypoint = data.y
        label.forEach((element, i) => {
            labels = { label: element, y: ypoint[i] };
            datapoints.push(labels)
            console.log("label2:", labels)
        });
        console.log("label3:", datapoints)
        return datapoints
    }
    render() {
        return (
            <div>
                <CanvasJSChart options={this.state.barchart} />
            </div>
        )
    }
}
class Doughnut extends Component {
    constructor(props) {
        super(props)
        this.state = {
            doughnut: {
                animationEnabled: true,
                title: {
                    text: "Work progress"
                },
                subtitles: [{
                    text: "90% Positive",
                    verticalAlign: "center",
                    fontSize: 24,
                    dockInsidePlotArea: true
                }],
                data: [{
                    type: "doughnut",
                    showInLegend: true,
                    indexLabel: "{name}: {y}",
                    yValueFormatString: "#,###'%'",
                    dataPoints: [
                        { name: "Unsatisfied", y: 5 },
                        { name: "Very Unsatisfied", y: 31 },
                        { name: "Very Satisfied", y: 40 },
                        { name: "Satisfied", y: 17 },
                        { name: "Neutral", y: 7 }
                    ]
                }]
            }
        }
    }
    render() {
        return (
            <div>
                <CanvasJSChart options={this.state.doughnut} />
            </div>
        )
    }
}

class Areachart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            areachart: {
                animationEnabled: true,
                exportEnabled: true,
                title: {
                    text: "Global Sales"
                },
                axisY: {
                    title: "Sales",
                    suffix: "%"
                },
                toolTip: {
                    shared: true,
                    reversed: true
                },
                legend: {
                    verticalAlign: "center",
                    horizontalAlign: "right",
                    reversed: true
                },
                data: [
                    {
                        type: "stackedArea100",
                        name: "US",
                        showInLegend: true,
                        xValueFormatString: "YYYY",
                        dataPoints: [
                            { x: new Date(2000, 0), y: 40 },
                            { x: new Date(2001, 0), y: 62 },
                            { x: new Date(2002, 0), y: 53 },
                            { x: new Date(2003, 0), y: 49 },
                            { x: new Date(2004, 0), y: 52 },
                            { x: new Date(2005, 0), y: 32 },
                            { x: new Date(2006, 0), y: 28 },
                            { x: new Date(2007, 0), y: 46 },
                            { x: new Date(2008, 0), y: 55 },
                            { x: new Date(2009, 0), y: 88 },
                            { x: new Date(2010, 0), y: 68 },
                            { x: new Date(2011, 0), y: 63 },
                            { x: new Date(2012, 0), y: 65 },
                            { x: new Date(2013, 0), y: 68 },
                            { x: new Date(2014, 0), y: 48 },
                            { x: new Date(2015, 0), y: 39 },
                            { x: new Date(2016, 0), y: 20 },
                            { x: new Date(2017, 0), y: 26 }
                        ]
                    },
                    {
                        type: "stackedArea100",
                        name: "France",
                        showInLegend: true,
                        xValueFormatString: "YYYY",
                        dataPoints: [
                            { x: new Date(2000, 0), y: 20 },
                            { x: new Date(2001, 0), y: 12 },
                            { x: new Date(2002, 0), y: 19 },
                            { x: new Date(2003, 0), y: 28 },
                            { x: new Date(2004, 0), y: 42 },
                            { x: new Date(2005, 0), y: 75 },
                            { x: new Date(2006, 0), y: 85 },
                            { x: new Date(2007, 0), y: 55 },
                            { x: new Date(2008, 0), y: 45 },
                            { x: new Date(2009, 0), y: 38 },
                            { x: new Date(2010, 0), y: 29 },
                            { x: new Date(2011, 0), y: 19 },
                            { x: new Date(2012, 0), y: 14 },
                            { x: new Date(2013, 0), y: 18 },
                            { x: new Date(2014, 0), y: 16 },
                            { x: new Date(2015, 0), y: 13 },
                            { x: new Date(2016, 0), y: 10 },
                            { x: new Date(2017, 0), y: 14 }
                        ]
                    },
                    {
                        type: "stackedArea100",
                        name: "India",
                        showInLegend: true,
                        xValueFormatString: "YYYY",
                        dataPoints: [
                            { x: new Date(2000, 0), y: 0 },
                            { x: new Date(2001, 0), y: 6 },
                            { x: new Date(2002, 0), y: 8 },
                            { x: new Date(2003, 0), y: 10 },
                            { x: new Date(2004, 0), y: 12 },
                            { x: new Date(2005, 0), y: 15 },
                            { x: new Date(2006, 0), y: 17 },
                            { x: new Date(2007, 0), y: 18 },
                            { x: new Date(2008, 0), y: 25 },
                            { x: new Date(2009, 0), y: 18 },
                            { x: new Date(2010, 0), y: 24 },
                            { x: new Date(2011, 0), y: 29 },
                            { x: new Date(2012, 0), y: 31 },
                            { x: new Date(2013, 0), y: 52 },
                            { x: new Date(2014, 0), y: 72 },
                            { x: new Date(2015, 0), y: 63 },
                            { x: new Date(2016, 0), y: 20 },
                            { x: new Date(2017, 0), y: 18 }
                        ]
                    }
                ]
            }
        }
    }
    render() {
        return (
            <CanvasJSChart options={this.state.areachart} />
        )
    }
}


export { Doughnut, Barchart, Areachart };