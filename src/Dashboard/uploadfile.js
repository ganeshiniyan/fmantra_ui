import React, { Component, useEffect, useState } from 'react';
import Dropzone, { useDropzone } from 'react-dropzone'

const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 16
};

const thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    width: 400,
    height: 100,
    padding: 4,
    boxSizing: 'border-box'
};

const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
};

const img = {
    display: 'block',
    width: 'auto',
    height: '100%'
};


function Previews(props) {
    const [files, setFiles] = useState([]);
    const [allFiles,setallfiles] = useState({});
    const [color, setcolor] = useState("");
    const [imageFile, setImageFile] =useState([])
    console.log("files ", files)
    console.log("allFiles ", allFiles)
    console.log("setFIles ", setFiles)
    console.log("props", props)
    const { getRootProps, getInputProps } = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            console.log("accepted files:", acceptedFiles)
            setallfiles({"files":acceptedFiles})
            setFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
        }
    });

    const changeable = (e, ele) => {
        console.log("e", e.target.value)
        setcolor(e.target.value);
        console.log("color and all file:",color,allFiles)
    }

    const clickable = (e) => {
        console.log("clcik", e)
        // allFiles.push(files[0])
        // allFiles.push(color)
        console.log("allFiles 2", imageFile)
        setallfiles({"color":color})
        console.log("color and all file:",color,allFiles)
    }

    const thumbs = files.map(file => (
        <div style={thumb} key={file.name}>
            <div style={thumbInner}>
                <img
                    src={file.preview}
                    style={img}
                />
                <input type="text" onChange={(e) => changeable(e, this)} />
                <button onClick={(e) => clickable(e)}>Click me! </button>
                <input />
            </div>
        </div>
    ));
    //   const onClick = e => {
    //     //   console.log("event:")
    //   }


    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
        console.log("files in useEffect:", files)
    }, [files]);

    return (
        <section>
            <div {...getRootProps()}>
                <input {...getInputProps()} />
                <p>Drag 'n' drop some files here, or click to select files</p>
            </div>
            <aside style={thumbsContainer}>
                {thumbs}
            </aside>
        </section>
    );
}

export default Previews

// class Preview extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             files: [],
//             allFiles: [],
//             color: "",
//         }
//     }

//     onDrop(files){
//         // const {dispatch} = this.props 
//        console.log("ondrop",files);// undefined
//     }


//     render() {
//         const {accept,dispatch} = this.props
//     console.log("dispatch"+dispatch);
//         return (
//             <div>
//                 <Dropzone onDrop={this.onDrop} accept={accept}>
//                     <div>Try dropping some files here, or click to select files to upload.</div>
//                 </Dropzone>
//             </div>
//         )
//     }
// }
// export default Preview