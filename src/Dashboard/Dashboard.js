import React, { Component } from 'react'
import {
    Button,
    Badge,
    ListGroup,
    ListGroupItem,
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import View from './view'
import '../App.css';
import { Barchart, Doughnut, Areachart } from './Barchart.js'
import $ from 'jquery';
import Home from './Home';
import { Route, Redirect, BrowserRouter, Switch, Link } from 'react-router-dom'
import AddProduct from './addproduct';
import AddToKart from './addToKart'
import Overview from './overview';
import ImageList from './imageList';
// import View from './view'

class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this);
        this.menubar = this.menubar.bind(this);
        this.state = {
            isOpen: false,
            menuClose: false,
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    menubar(event) {
        console.log("clicking menu:", this.state.menuClose)
        // document.getElementsByClassName('menu-list')[0].style.visibility = 'hidden';
        if (this.state.menuClose === false) {
            $(".menu-list").hide();
            $('.main-char-div').removeClass("col-md-10")
            $('.main-char-div').addClass("col-md-12")
            this.setState({ menuClose: true });
        }
        else if (this.state.menuClose === true) {
            $(".menu-list").show();
            $('.main-char-div').removeClass("col-md-12")
            $('.main-char-div').addClass("col-md-10")
            this.setState({ menuClose: false })
        }
    }

    //this for fmantra start



    //this is for fmantra end

    render() {

        return (
            <div>
                <div className="app-bar">
                    <div className="col-md-2 headers">
                        <img className="dashboardlogo" src="Images/dummy logo.svg" alt="dummy" />
                    </div>
                    <div className="col-md-10 headers">
                        <div className="app-bar-button">
                            <span>
                                <i class="fa fa-bars" onClick={this.menubar} aria-hidden="true"></i>
                                <label className="menu-title">Dashboard</label>
                            </span>
                            <div className="app-bar-icons">
                                <span className="notification">
                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                    <Badge color="danger">4</Badge>
                                </span>
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main-dashboard">
                    <div className="col-md-2 menu-list">
                        <ul>
                            <li className="nav-item active">
                                <Link to="/home" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>Home</span>
                                </Link>
                                <Link to="/view" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>View</span>
                                </Link>
                                <Link to="/addtokart" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>AddToKart</span>
                                </Link>
                                <Link to="/overview" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>Overview</span>
                                </Link>
                            </li>
                        </ul>
                        {/* <ListGroup>
                            <ListGroupItem className="list-item" active tag="a" href="/dashboard" action>Home</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Dapibus ac facilisis in</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Morbi leo risus</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Porta ac consectetur ac</ListGroupItem>
                            <ListGroupItem className="list-item" disabled tag="a" href="/dashboard" action>Vestibulum at eros</ListGroupItem>
                        </ListGroup> */}

                        {/* <BrowserRouter>
                            <Switch>
                                <Route exact path="/Home" component={Home} /> */}
                                {/* <Route path='/' render={()=>
                          <Redirect to="/dashboard" />} /> */}
                            {/* </Switch>
                        </BrowserRouter> */}
                    </div>
                    {/* <div className="col-md-10 main-char-div" style={{ display: "flex" }}> */}
                    {/* <div className="chart-div">
                            <div className="col-md-6 bar-chart">
                                <Barchart />
                            </div>
                            <div className="col-md-6 doughnut-chart">
                                <Doughnut />
                            </div>
                        </div>
                        <div className="chart-div area-chart">
                            <Areachart />
                        </div> */}

                    {/* </div> */}
                    <div className="col-md-10" style={{ padding: "0px" }}>
                    {/* <ImageList /> */}
                    {/* <Overview /> */}
                        <AddProduct />
                        {/* <View /> */}
                        {/* <AddToKart /> */}
                    </div>
                </div>
            </div>
        )
        return (
            <div>fm</div>
        )
    }
}

export default Dashboard;