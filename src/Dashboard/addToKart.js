import React, { Component } from 'react'
import {
    Button,
    Badge,
    ListGroup,
    ListGroupItem,
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import '../App.css';
import $ from 'jquery';
import Home from './Home';
import { Route, Redirect, BrowserRouter, Switch, Link } from 'react-router-dom'
import AddProduct from './addproduct';
// import AddToKart from './addToKart'

class AddToKart extends Component {
    constructor(props) {
        super(props)
        // this.toggle = this.toggle.bind(this);
        this.menubar = this.menubar.bind(this);
        this.state = {
            isOpen: false,
            menuClose: false,
        };
    }

    menubar(event) {
        console.log("clicking menu:", this.state.menuClose)
        // document.getElementsByClassName('menu-list')[0].style.visibility = 'hidden';
        if (this.state.menuClose === false) {
            $(".menu-list").hide();
            $('.main-char-div').removeClass("col-md-10")
            $('.main-char-div').addClass("col-md-12")
            this.setState({ menuClose: true });
        }
        else if (this.state.menuClose === true) {
            $(".menu-list").show();
            $('.main-char-div').removeClass("col-md-12")
            $('.main-char-div').addClass("col-md-10")
            this.setState({ menuClose: false })
        }
    }

    render() {
        return (
            <div>
                <div className="app-bar">
                    <div className="col-md-2 headers">
                        <img className="dashboardlogo" src="Images/dummy logo.svg" alt="dummy" />
                    </div>
                    <div className="col-md-10 headers">
                        <div className="app-bar-button">
                            <span>
                                <i class="fa fa-bars" onClick={this.menubar} aria-hidden="true"></i>
                                <label className="menu-title">Dashboard</label>
                            </span>
                            <div className="app-bar-icons">
                                <span className="notification">
                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                    <Badge color="danger">4</Badge>
                                </span>
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main-dashboard">
                    <div className="col-md-2 menu-list">
                        <ul>
                            <li className="nav-item active">
                                <Link to="/home" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>Home</span>
                                </Link>
                                <Link to="/view" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>View</span>
                                </Link>
                                <Link to="/addtokart" className="nav-link">
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span>AddToKart</span>
                                </Link>
                            </li>
                        </ul>
                        {/* <ListGroup>
                            <ListGroupItem className="list-item" active tag="a" href="/dashboard" action>Home</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Dapibus ac facilisis in</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Morbi leo risus</ListGroupItem>
                            <ListGroupItem className="list-item" tag="a" href="/dashboard" action>Porta ac consectetur ac</ListGroupItem>
                            <ListGroupItem className="list-item" disabled tag="a" href="/dashboard" action>Vestibulum at eros</ListGroupItem>
                        </ListGroup> */}

                        <BrowserRouter>
                            <Switch>
                                <Route exact path="/Home" component={Home} />
                                {/* <Route path='/' render={()=>
                          <Redirect to="/dashboard" />} /> */}
                            </Switch>
                        </BrowserRouter>
                    </div>
                    {/* <div className="col-md-10 main-char-div" style={{ display: "flex" }}> */}
                    {/* <div className="chart-div">
                            <div className="col-md-6 bar-chart">
                                <Barchart />
                            </div>
                            <div className="col-md-6 doughnut-chart">
                                <Doughnut />
                            </div>
                        </div>
                        <div className="chart-div area-chart">
                            <Areachart />
                        </div> */}

                    {/* </div> */}
                    <div className="col-md-10 main-char-div" style={{ padding: "0px" }}>
                        <div className="col-md-12 view" style={{ display: "flex", padding: "0px" }}>
                            <div className="col-md-7">
                                <div style={{ display: "flex", border: "1px solid #d6d6c0", padding: "0px", boxShadow: "0 2px 5px 0 rgba(0, 0, 0, 0.26", height: "200px" }}>
                                    <div className="col-md-3" style={{ height: "200px" }}>
                                        <img src="Images/blue.jpg" style={{ height: "200px" }} />
                                    </div>
                                    <div className="col-md-5">
                                        <div style={{ fontSize: "20px" }}>Monochrome Striped Shirt</div>
                                        <div style={{ fontSize: "13px" }}>by kay Dee</div>
                                        <div style={{ marginTop: "20px" }}>Size: M</div>
                                        <div style={{ marginTop: "20px" }}>Quantity: 1</div>
                                    </div>
                                    <div className="col-md-4">
                                        <i class="fa fa-times" style={{ float: "right" }}></i>
                                        <div style={{ clear: "both", float: "right", marginTop: "20px" }}>
                                            <span>₹429</span>
                                        </div>
                                        <div style={{ clear: "both", float: "right", fontSize: "14px" }}>
                                            + ₹49 (Shipping Charges)
                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4" style={{ padding: "0px", border: "1px solid #d6d6c0", marginLeft: "40px", boxShadow: "0 2px 5px 0 rgba(0, 0, 0, 0.26", height: "100vh" }}>
                                <div style={{ fontSize: "20px" }}>
                                    SUMMARY
                    </div>
                                <div style={{ display: "flex", marginTop: "20px" }}>
                                    <div className="col-md-8">
                                        <div style={{ fontSize: "15px" }}>
                                            <span>Total Price</span>
                                        </div>
                                        <div style={{ fontSize: "15px" }}>
                                            <span>Shipping Charges</span>
                                        </div>
                                        <div style={{ fontSize: "20px", color: "#D3145A" }}>
                                            <span>Amount Payable</span>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div style={{ clear: "both", float: "right", fontSize: "15px" }}>
                                            <span>₹429</span>
                                        </div>
                                        <div style={{ clear: "both", float: "right", fontSize: "15px" }}>
                                            <span>+ ₹49</span>
                                        </div>
                                        <div style={{ clear: "both", float: "right", fontSize: "20px", color: "#D3145A" }}>
                                            <span>₹478</span>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <Button style={{ width: "94%", background: "#D3145A", marginLeft: "10px", marginTop: "20px" }}>Check out</Button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            // ===========================
        )
    }
}

export default AddToKart