import React, { Component } from 'react'
import axios from 'axios'


class Overview extends Component {
    constructor(props) {
        super(props)
        this.state = {
            maindata: []
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:3900/api/products/getByPage/1/10`, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjYTBiNmMwOTEwMDcyNTg5NTNmYjk3NCIsImlhdCI6MTU1NDUyMTQzMiwiZXhwIjoxNTU0NjA3ODMyfQ.XQrABeq4horESr7Ib7MPsBKLZPLZXCdMHmYLuvaUU9U"
            }
        }).then((response) => {
            console.log("respose:", response)
            this.setState({ maindata: response.data.data.docs })
        }).catch((err) => {
            console.log(err);
        });
    }
    render() {
        console.log("this.state:", this.state)
        let data = this.state.maindata;
        console.log("data", this.state.maindata)
        return (
            <div className="row">
                {
                    data.map((ele, i) => {
                        console.log("element overview:", ele)
                        return (
                            <div className="col-md-4" style={{ height: "400px", width: "400px" }}>
                                <div>
                                    {ele.subcategory}
                                </div>
                                <div style={{ display: "flex" }}>
                                    <div className="col-md-8" style={{ height: "300px", padding: "0px", paddingRight: "5px" }}>
                                        <img src={ele.fileDescriptions[0].file.preview} style={{ height: "300px" }} />
                                    </div>
                                    <div className="col-md-4" style={{ height: "300px", padding: "0px", paddingRight: "5px" }}>
                                        <img src={ele.fileDescriptions[1].file.preview} style={{ height: "100px" }} />
                                        <img src={ele.fileDescriptions[2].file.preview} style={{ height: "100px" }} />
                                        <img src={ele.fileDescriptions[3].file.preview} style={{ height: "100px" }} />
                                    </div>
                                </div>
                                <div>
                                    <div>
                                    <span>{ele.brandName}</span>
                                    <span style={{float:"right"}}><i class="fa fa-heart-o" aria-hidden="true"></i>40 likes</span>
                                    </div>
                                    <div>
                                    {ele.description}
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )

    }
}

export default Overview