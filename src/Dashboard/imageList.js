import React, { Component } from 'react'
import axios from 'axios'


class ImageList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            maindata: []
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:3900/api/products/getByPage/1/10`, {
            headers: {
                "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjYTBiNmMwOTEwMDcyNTg5NTNmYjk3NCIsImlhdCI6MTU1NDUyMTQzMiwiZXhwIjoxNTU0NjA3ODMyfQ.XQrABeq4horESr7Ib7MPsBKLZPLZXCdMHmYLuvaUU9U"
            }
        }).then((response) => {
            console.log("respose:", response)
            this.setState({ maindata: response.data.data.docs })
        }).catch((err) => {
            console.log(err);
        });
    }
    render() {
        console.log("this.state:", this.state)
        let data = this.state.maindata;
        console.log("data", this.state.maindata)
        return (
            <div className="row">
                {
                    data.map((ele, i) => {
                        console.log("element overview:", ele)
                        return (
                            <div className="col-md-3" style={{height: "380px",width: "350px"}}>
                                <div style={{ display: "flex" }}>
                                    <div className="col-md-12" style={{padding: "0px", paddingRight: "5px" }}>
                                        <img src={ele.fileDescriptions[0].file.preview} style={{ height: "260px"}} />
                                    </div>
                                </div>
                                <div>
                                    <div>
                                    <span>{ele.brandName}</span>
                                    <span style={{float:"right"}}><i class="fa fa-heart-o" aria-hidden="true"></i>40 likes</span>
                                    </div>
                                    <div>
                                    Price:{ele.wholeSalePrice}
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )

    }
}

export default ImageList