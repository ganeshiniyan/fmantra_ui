import axios from 'axios';
import { BASE_URL } from './constants';

export const register = (data) => {
    return axios.post(`${BASE_URL}/auth/register`, data);
}
