export const { BASE_URL } = window;


export const FormType = {
    DEFAULT: "DEFAULT",
    STEP: "STEP",
    SEARCH: "SEARCH"
}



export const REGISTRATION_FORM = {
    title: "Welcome",
    elements: [
        {
            title: "Category",
            name: "selectCategory",
            label: "select One",
            type: "select",
            props: {
                required: true
            },
            options: [
                {
                    value: "Select Category",
                    label: "Select Category"
                },
                {
                    value: "Kids",
                    label: "kids"
                },
                {
                    value: "Mens",
                    label: "mens"
                },
                {
                    value: "Womens",
                    label: "womens"
                }
            ]
        },
        {
            title: "Sub-Category",
            name: "selectSubCategory",
            label: "select One",
            type: "select",
            props: {
                required: true
            },
            options: [
                {
                    value: "Select Sub-Category",
                    label: "Select Sub-Category"
                },
                {
                    value: "Boys clothing",
                    label: "One"
                },
                {
                    value: "Girls clothing",
                    label: "Two"
                },
            ]
        },
        {
            title: "Select Type",
            name: "selectType",
            label: "select One",
            type: "select",
            props: {
                required: true
            },
            options: [
                {
                    value: "Select Type",
                    label: "Select Type"
                },
                {
                    value: "DEALS OF THE DAY",
                    label: "One"
                },
                {
                    value: "SPECIAL OFFERS",
                    label: "Two"
                },
                {
                    value: "NEW ARRIVAL",
                    label: "Three"
                },
            ]
        },
        {
            title: "Product Code",
            name: "productCode",
            label: "Product Code",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title:"product Name",
            name: "productName",
            label: "Product Name",
            type: "text",
            props: {
                required: true,
                placeholder: "UserName"
            }
        },
        {
            title: "Select Brand",
            name: "selectBrand",
            label: "Please select brand",
            type: "select",
            props: {
                required: true
            },
            options: [
                {
                    value: "Select Brand",
                    label: "Select Brand"
                },
                {
                    value: "PUMA",
                    label: "Two"
                },
                {
                    value: "ADIDAS",
                    label: "Three"
                },
                {
                    value: "Nike",
                    label: "four"
                }
            ]
        },
        // {
        //     title: "Size",
        //     name: "Size",
        //     label: "Size ratio",
        //     // type: "text",
        //     type2:"number",
        //     name2: "quantity",
        //     props: {    
        //         // required: true,
        //         // placeholder: ""
        //     }
        // },
        {
            name:"SIZE_FORM",
            title: "Size Ratio",
            type:"sizeratio",
            placeholder:"size",
            placeholder2:"quantity"
        },
        {
        name:"LOI_FORM",
        title: "Color Upload",
        type:"colorUpload",
        placeholder:"Type the colour"
        },
        {
            title: "Retail Price",
            name: "retailPrice",
            label: "Retail Price",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title: "Retail MOQ",
            name: "retailMoq",
            label: "Retail MOQ",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title: "Wholesale Price",
            name: "wholesalePrice",
            label: "Wholesale Price",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title: "Wholesale MOQ",
            name: "wholesaleMoq",
            label: "Wholesale MOQ",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title: "Description",
            name: "description",
            label: "Description",
            type: "textarea",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title: "Stocks",
            name: "stocks",
            label: "Stocks",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
        {
            title: "Quality",
            name: "quailty",
            label: "Quality",
            type: "select",
            props: {
                required: true
            },
            options: [
                {
                    value: "Select Quality",
                    label: "Select Quality"
                },
                {
                    value: "good",
                    label: "Good"
                },
                {
                    value: "average",
                    label: "Average"
                },
                {
                    value: "premium",
                    label: "Premium"
                },
            ]
        },
        {
            title: "Product Type",
            name: "productType",
            label: "Product Type",
            type: "select",
            props: {
                required: true
            },
            options: [
                {
                    value: "Select Product Type",
                    label: "Select Product Type"
                },
                {
                    value: "buy",
                    label: "Buy"
                },
                {
                    value: "soldOut",
                    label: "Sold Out"
                },
                {
                    value: "comingSoon",
                    label: "Coming Soon"
                },
            ]
        },
        {
            title: "GST",
            name: "gst",
            label: "GST",
            type: "text",
            props: {
                required: true,
                placeholder: "Name"
            }
        },
    ]
}