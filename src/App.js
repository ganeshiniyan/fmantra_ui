import React, { Component } from 'react';
// import Login from './login/login'
import logo from './logo.svg';
import './App.css';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios'
// import { connect } from 'react-redux';
import Dashboard from './Dashboard/Dashboard';
import View from './Dashboard/view';
import { Route,Redirect, BrowserRouter, Switch } from 'react-router-dom'
import AddToKart from './Dashboard/addToKart';
import Overview from './Dashboard/overview';
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      password: "",
      flag: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlepass = this.handlepass.bind(this)
    this.submit = this.submit.bind(this);
  }
  handleChange(event) {
    console.log("inside change:", event.target.value)
    this.setState({ name: event.target.value });
  }
  handlepass(event) {
    console.log("inside change:", event.target.value)
    this.setState({ password: event.target.value });
  }

  submit() {
    var _this = this;
    console.log("A name was submitted: " + this.state.name + this.state.password);
    // axios.post('http://localhost:3001/login', {
    //   Name: this.state.name,
    //   Password: this.state.password
    // })
    //   .then(function (response) {
    //     // console.log("data:", response.data.Name);
    //     if (response.data == "valid credential") {
    //       console.log("valid :", response.data)
          _this.setState({ flag: true })
      //   }

      // })
      // .catch(function (error) {
      //   console.log(error);
      // });
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.name);
    console.log("A name was submitted: " + this.state.name);
    // event.preventDefault();
  }
  render() {
    // <Login />
    if (this.state.flag == false) {
      return (
        <div className="App">
          <header className="App-header">
            {/* <img src="/Images/background@6d.jpg" /> */}
            <nav className="navbar navbar-expand-md main-navbar fixed-top d-print-block" style={{ background: "#ffffff", boxShadow: "none", height: "70px" }}>
              <a className="navbar-brand" href="#">
                <img src="Images/dummy logo.svg" style={{ height: "50px" }} alt="dummy" />
              </a>
              <Button className="primary" color="primary">Register</Button>
            </nav>
            <div className="login-part">
              <div className="login-text">
                Login
              </div>
              <Form>
                <FormGroup>
                  <Label className="label" for="exampleEmail">User Name</Label>
                  <Input className="input" type="text" name="username" value={this.state.name} onChange={this.handleChange} placeholder="Username" />
                </FormGroup>
                <FormGroup>
                  <Label className="label" for="examplePassword">Password</Label>
                  <Input className="input" type="password" name="password" value={this.state.password} onChange={this.handlepass} placeholder="password" />
                </FormGroup>
                <FormGroup check>
                  <Label className="label" >
                    <Input type="checkbox" className="checkbox" />{' '}
                    Check me out
                  </Label>
                </FormGroup>
                <Button type="button" className="login" onClick={this.submit} color="primary">
                  Login</Button>
              </Form>
            </div>
          </header>
        </div>
      )
    }
    else if (this.state.flag == true) {
      return (
        // <Route path='/two' render={() => <Two />}/>
        <BrowserRouter>
          <Switch>
          <Route exact path="/overview" component={Overview} />
          <Route exact path="/view" component={View} />
          <Route exact path="/addtokart" component={AddToKart} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route path='/' render={()=>
          <Redirect to="/dashboard" />} />
          </Switch>
        </BrowserRouter>
        // <Route exact path="/" render={() => 
          // (
          // loggedIn ? (
            // <Redirect to="/dashboard" />
          // ) : (
          //     <PublicHomePage />
          //   )
        // )} />
        // <div>
        //   <Redirect to="/dashboard"/>
        //   <p>logged In</p>
        // </div>
      )
    }
  }
}

export default App;
